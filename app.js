import { Circle } from "./class/Circle.js";
import { circle_list } from "./data.js";

const broadcastChannel = new BroadcastChannel('randomElementChannel');

broadcastChannel.onmessage = (event) => {
	const DATA = event.data;
	console.log(DATA);

	if (DATA.type === "createRandomElement") {
		console.log("Success create new element");
	}
} 

const COUNT = 5;

for (let i=0; i<COUNT; i++) new Circle().add();

console.log(circle_list);