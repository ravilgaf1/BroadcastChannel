import { circle_list, color } from "../data.js";


export class Circle {
	size = this.#randomSize(30, 60);
	color = this.#randomColor();

	list() {
		return circle_list;
	}

	add() {
		const broadcastChannel = new BroadcastChannel('randomElementChannel');
		const SIZE = this.size + "vh";
		const ELEMENT = { class: this, size: SIZE, color: this.color };
		circle_list.push(ELEMENT);
		
		const node = document.createElement("div");
		node.classList.add("circle")
		node.style.width = SIZE;
		node.style.height = SIZE;
		node.style.backgroundColor = this.color;

		document.body.appendChild(node);

		broadcastChannel.postMessage({ type: "createElement", value: ELEMENT });
	}

	#randomColor() {
		const INDEX = Math.floor(Math.random() * color.length);
		const COLOR = color[INDEX];
		color.splice(INDEX, 1);

		return COLOR;
	}

	#randomSize(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}